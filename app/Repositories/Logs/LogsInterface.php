<?php

namespace App\Repositories\Logs;

interface LogsInterface
{
    public function store($file);

    public function filter($request);

}
