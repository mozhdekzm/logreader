<?php

namespace App\Repositories\Logs;

use \App\Models\Log;
use Carbon\Carbon;

class Logs implements LogsInterface
{
    /**
     * @param $file
     */
    public function store($file)
    {
        foreach (file($file) as $line) {
            $log = explode(" ", $line);
            Log::create([
                "service_name" => $log[0],
                "service_method" => ltrim($log[3], '"'),
                "service_url" => $log[4],
                "request_type" => $log[5],
                "response_status_code" => $log[6],
                "created_at" => Carbon::createFromFormat("d/M/Y:h:m:s", rtrim(ltrim($log[2], '['), ']'))->toDateTime()
            ]);
        }
    }

    public function filter($request)
    {
        return
            Log::when(isset($request['serviceNames']), function ($q) use ($request) {
                $q->whereIn('service_name', $request['serviceNames']);
            })
                ->when(isset($request['statusCode']), function ($q) use ($request) {
                    $q->where('response_status_code', $request['statusCode']);
                })
                ->when(isset($request['startDate']), function ($q) use ($request) {
                    $q->whereDate('created_at', '>=', $request['startDate']);
                })
                ->when(isset($request['endDate']), function ($q) use ($request) {
                    $q->whereDate('created_at', '<', $request['endDate']);
                })
                ->count();
    }
}
