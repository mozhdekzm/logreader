<?php

namespace App\Providers;

use App\Services\Logs\LogsService;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Logs\Logs;
use App\Repositories\Logs\LogsInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(
            LogsInterface::class,
            Logs::class
        );

    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
