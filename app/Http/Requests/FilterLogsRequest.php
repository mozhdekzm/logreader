<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class FilterLogsRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'serviceNames' => 'nullable|array',
            'statusCode' => 'nullable|integer',
            'startDate' => 'nullable|string',
            'endDate' => 'nullable|string',
        ];
    }
}
