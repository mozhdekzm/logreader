<?php


namespace App\Http\Controllers;


use App\Http\Requests\FilterLogsRequest;
use App\Repositories\Logs\LogsInterface;

class LogController extends Controller
{
    /**
     * @var LogsInterface
     */
    private $logsRepository;

    public function __construct(LogsInterface $logs)
    {
        $this->logsRepository = $logs;
    }

    public function filterLogs(FilterLogsRequest $request)
    {
        return ["count" => $this->logsRepository->filter($request->all())];
    }
}
