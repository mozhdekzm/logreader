<?php

namespace App\Console\Commands;

use App\Repositories\Logs\LogsInterface;
use Illuminate\Console\Command;

class StoreLog extends Command
{

    private $logsRepository;

    public function __construct(LogsInterface $logsRepository)
    {

        parent::__construct();
        $this->logsRepository = $logsRepository;
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command parse and store the log file that existed in public';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->logsRepository->store("public/logs.txt");
    }
}
