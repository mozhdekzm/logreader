# LogsReader project

this is an project that you can parse and store the data of log.txt that it is in public directory to logs table and you can set some filters on this data

## Running

I use laravel sail to install and run the project so for runnig you should do this:

```bash
cd {logsReader-directory}

./vendor/bin/sail up
```
## create migrations
you can add migrations with :

```bash
 ./vendor/bin/sail artisan migrate
```
you can connect to phpmyadmin from http://localhost:8080/ and the username and password for db is in .env.example



## Store:log Command
for saving data in logs table you can run this command


```php
 ./vendor/bin/sail artisan store:log
```

## Filter api

we have an api for filter the logs data

url:
```php
 http://localhost/api/logs/count
```
method :
```php
 GET
```
Body sample:
```json
 {
    "serviceNames":[
        "order-service",
        "invoice-service"
    ],
    "statusCode":"422",
    "startDate":"2023-09-16 10:00:53",
    "endDate":"2024-09-16 10:00:53"
}
```
and the response is like this :
```json
 {
    "count": 12
}
```
the curl request of this api is
```curl
 curl --location --request GET 'http://localhost/api/logs/count' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--header 'Cookie: XSRF-TOKEN=eyJpdiI6IktYWno2OWJnY1BTV0xNOXZWR25meXc9PSIsInZhbHVlIjoiMmx5bEo5dkRSczlzRUNmM05pZ1V5dE42R1pnMmp5M2l1dzFBZXM3OTFiNWpXaGtGd3JZemVLRWFINlZqVzVmN3VKY3JZUHkvRjhHb3I3S1N3Q1d0dEkwTGE5M3lRR3E4dkoraHFLZkJySU5RZlZOWXh1K0FjcDdZazRjVWxiVGsiLCJtYWMiOiJhYzU4MjhiYTMxMWY3NWE4OTg3NWY2NTYyZTM5ZTY0MWM5YmFlYzEyYzFmNWFhNDI4OTkzYTJmMTdkYzJjZjdkIiwidGFnIjoiIn0%3D; laravel_session=eyJpdiI6IkdpZE5ERWlaSzlVR1FTc0JhQWxKQlE9PSIsInZhbHVlIjoiRE5nbEt4UEh3b0JuZW91TkVYRDlaQlE2bjlmOVY3T3FMSVhPUUVhMEYvUVpjTmpIQm13Zi9kMmdyaHpVM3ZLdWlUUENRbGJSZzFVczhxdTh5UU41YVhKTUtMcFVZRlFJdTUzUTNNYmZtUEovM3IydENnSGVHaGU4ZmZ0OGxlNUYiLCJtYWMiOiIxODEzMzljNGY5MjQ5ZjM2MzNlY2JiNGI3MTNlMTk4NjE3NzA1ZmFmMzYzMmJiZmFlYzNhNDQxNTBmMjUxODA1IiwidGFnIjoiIn0%3D' \
--data-raw '{
    "serviceNames":[
        "order-service",
        "invoice-service"
    ],
    "statusCode":"422",
    "startDate":"2023-09-16 10:00:53",
    "endDate":"2024-09-16 10:00:53"
}'
```

you can add this to postman and call it after running the project

## testing

for testing the command store:log I add a feature test 
for running the test first you should change mode to testing like this
```json
 ./vendor/bin/sail artisan config:cache --env=testing

./vendor/bin/sail artisan cache:clear --env=testing
```
then:
```json
 ./vendor/bin/sail artisan test
```
